import { Component, OnInit, Input } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@Component({
  selector: 'app-leftnav',
  templateUrl: './leftnav.component.html',
  styleUrls: ['./leftnav.component.css']
})
export class LeftnavComponent implements OnInit {

public burgerShow: string;
  
  constructor() {
    this.burgerShow = 'Expand';
  }

  ngOnInit() {
  }

  notBurgerShow() {return this.burgerShow = (this.burgerShow === 'Expand') ? 'Collapse' : 'Expand'}
}
