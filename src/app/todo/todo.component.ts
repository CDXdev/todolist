import { Component, OnInit, Input } from '@angular/core';
import { Element, MainComponent } from '../main/main.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})

export class TodoComponent implements OnInit {
  @Input() todoList: Array<Element>;
  public numTodo: number;

  constructor(private route: ActivatedRoute) {
    this.numTodo = +this.route.snapshot.paramMap.get('num');
    /*if (this.numTodo > this.todoList.length) {
      // redirect page not found
    }*/
  }

  ngOnInit() {
  }
}
