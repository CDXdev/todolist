import { Component, OnInit } from '@angular/core';

enum Status {ONGOING, COMPLETED, PRIORITY}

export class Element {
  constructor(public num: number, public title: string, public check: boolean, public status: Status, public visible: boolean) {};
  setAsCompleted() {this.status = Status.COMPLETED; }
  setAsVisible(visible: boolean) {this.visible = visible; }
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit {

  public todoList: Array<Element>;
  public ishosCompleted;
  public ctrlHosClass;

  constructor() {
    this.todoList = [
        new Element(1, 'Angular flex layout ver2', false, Status.COMPLETED, true),
        new Element(2, 'routing', false, Status.COMPLETED, true),
        new Element(3, 'API d\'appels HTTP', false, Status.ONGOING, true),
        new Element(4, 'Angular material : animation', false, Status.ONGOING, true),
        new Element(5, 'Angular CLI : faire evoluer les tests unitaires', false, Status.ONGOING, true)];
    this.ishosCompleted = 'Show';
    this.ctrlHosClass = 'ctrlHide';
  }

  ngOnInit() {
  }

  hosCompleted() {
    this.ishosCompleted = (this.ishosCompleted === 'Hide') ? 'Show' : 'Hide';
    this.todoList.forEach(i => {
      if (i.status === Status.COMPLETED) {
        i.setAsVisible(this.ishosCompleted !== 'Show');
      }
    });
    console.log('hosCompleted() called');
  }

  hosControls() {
    let show = false;
    this.todoList.forEach(i => {
      if (i.check) {
        show = true;
      }
    });
    this.ctrlHosClass = (show) ? 'ctrlShow' : 'ctrlHide';
    console.log('hosControls() called');
  }

  addElement(inputValue: string) {
    if (inputValue !== '') {
      this.todoList.push(new Element(1 + this.todoList.length, inputValue, false, Status.ONGOING, true));
    }
    console.log('addElement() called');
  }

  completed() {
    this.todoList.forEach(i => {
      if (i.check) {
        i.setAsCompleted();
      }
    });
    console.log('completed() called');
  }

  deletee() {
    this.todoList.forEach(i => {
      if (i.check) {
        this.todoList.splice(this.todoList.indexOf(i), 1);
      }
    });
    console.log('deletee() called');
  }

  getStatus(index) {
    return Status[index];
  }

  saveHttp(){
    // todo
  }
}
